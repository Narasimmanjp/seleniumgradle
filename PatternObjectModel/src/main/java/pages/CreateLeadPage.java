package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "createLeadForm_companyName")
	WebElement eleCompanyName;
	@And("Enter the CompanyName as(.*)")
	public CreateLeadPage enterCompanyName(String data) {		
		type(eleCompanyName, data);
		return this;
	}
	
	@FindBy(how=How.ID,using ="createLeadForm_firstName")
	WebElement eleFirstName;
	@And("Enter the FirstName as (.*)")
	public CreateLeadPage enterFirstName(String data) {
		type(eleFirstName, data);
		return this;		
	}
	
	@FindBy(how=How.ID,using ="createLeadForm_lastName")
	WebElement eleLastName;
	@And("Enter the LastName as(.*)")
	public CreateLeadPage enterLastName(String data) {
		type(eleLastName, data);
		return this;		
	}
	
	@FindBy(how=How.ID,using ="createLeadForm_generalProfTitle")
	WebElement eleTitle;
	
	public CreateLeadPage enterTitle(String data) {
		type(eleTitle, data);
		return this;		
	}
	@FindBy(how=How.ID,using ="createLeadForm_primaryPhoneNumber")
	WebElement elePhoneNumber;
	
	public CreateLeadPage enterPhoneNumber(String data) {
		type(elePhoneNumber, data);
		return this;		
	}
	
	
	
	@FindBy(how=How.CLASS_NAME,using ="smallSubmit")
	WebElement eleCreateLeadButton;
	@And("Click on CreateButton")
	public ViewLeadPage clickCreateLead() {
		click(eleCreateLeadButton);
		return new ViewLeadPage();		
	}
	
	public CreateLeadPage clickCreateLeadFailedCase() {
		click(eleCreateLeadButton);
		return this;		
	}
	
	@FindBy(how=How.XPATH,using ="//li[@class='errorMessage']")
	WebElement eleErrorMessage;
	
	public CreateLeadPage verifyErrorMessage() {
		verifyPartialText(eleErrorMessage, "parameter is missing");
		return this;
	}
	
	
	
}
