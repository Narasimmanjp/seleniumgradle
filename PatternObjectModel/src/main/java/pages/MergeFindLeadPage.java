package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class MergeFindLeadPage extends ProjectMethods{

	public MergeFindLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//label[text()='Lead ID:']/following::input")
	WebElement eleLeadID;
	@And("Enter LeadId to Find as (.*)")
	public MergeFindLeadPage enterLeadId(String data) {		
		type(eleLeadID, data);
		return this;
	}
	
	@FindBy(how = How.XPATH, using = "//label[text()='First name:']/following::input")
	WebElement eleFirstName;
	@And("Enter FirstName to Find as(.*)")
	public MergeFindLeadPage enterFirstName(String data) {		
		type(eleFirstName, data);
		return this;
	}
	
	@FindBy(how = How.XPATH, using = "//label[text()='Last name:']/following::input")
	WebElement eleLastName;
	@And("Enter LastName to Find as (.*)")
	public MergeFindLeadPage enterLastName(String data) {		
		type(eleLastName, data);
		return this;
	}
	
	@FindBy(how = How.XPATH, using = "//label[text()='Company Name:']/following::input")
	WebElement eleCompanyName;
	@And("Enter CompanyName to Find as (.*)")
	public MergeFindLeadPage enterCompanyName(String data) {		
		type(eleCompanyName, data);
		return this;
	}
		
	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']")
	WebElement eleFindLead;
	@And("Click Find Lead button")
	public MergeFindLeadPage clickFindLead() {		
		click(eleFindLead);
		return this;
	}
	
	@FindBy(how = How.XPATH, using = "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	WebElement eleFirstLead;
	@And("Click on FirstResultingLead")
	public MergeLeadPage clickFirstResultingLead() {
		click(eleFirstLead);
		return new MergeLeadPage();
	}
	@FindBy(how = How.XPATH, using = "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	WebElement eleFirstLead1;
	public MergeLeadPage clickFirstResultingLeadtomerge() {
		click(eleFirstLead1);
		switchToWindow(0);
		return new MergeLeadPage();
	}
		
	
}
