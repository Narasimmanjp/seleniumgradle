package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC006_MergeLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC006_MergeLead";
		testDescription = "Merge two Existing Leads";
		authors = "Narasimman";
		category = "smoke";
		dataSheetName = "TC006_MergeLead";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void CreateLead(String userName,String password,String Lead1FindbyID,String Lead1FindbyFirstName,
			String Lead1FindbyLastName,String Lead1FindbyCompany,String Lead2FindbyID,String Lead2FindbyFirstName,
			String Lead2FindbyLastName,String Lead2FindbyCompany)
	{		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickMergeLead()
		.clickFromLead()
		.enterLeadId(Lead1FindbyID)
		.enterCompanyName(Lead1FindbyCompany)
		.enterFirstName(Lead1FindbyFirstName)
		.enterLastName(Lead1FindbyLastName)
		.clickFindLead()
		.clickFirstResultingLeadtomerge()
		
		.clickToLead()
		.enterLeadId(Lead2FindbyID)
		.enterCompanyName(Lead2FindbyCompany)
		.enterFirstName(Lead2FindbyFirstName)
		.enterLastName(Lead2FindbyLastName)
		.clickFindLead()
		.clickFirstResultingLeadtomerge()
		
		.clickMerge()
		.acceptToMerge()
		
		
		
		
		;
		
		
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	
}
